import { action } from '@storybook/addon-actions'
import 'leaflet/dist/leaflet.css'
import './styles.css'

import { tileLayer } from 'leaflet'

import Map from '../src/Map.svelte'
import ActionView from './Action.view.svelte'

export default {
  title: 'Map',
}

export const Action = () => ({
  Component: ActionView,
  on: {
    click: action('click'),
  },
})

export const Component = () => ({
  Component: Map,
  props: {
    center: [51.505, -0.09],
    zoom: 13,
  },
  on: {
    ready: e => {
      action('ready')(e)
      const { detail: map } = e
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(map)

      map.on('click', action('click'))
    },
  },
})
