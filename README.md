# leaflet-svelte

Svelte Leaflet integration

## Installation

```bash
yarn add leaflet
yarn add -D leaflet-svelte
```

```bash
npm i leaflet
npm i -D leaflet-svelte
```

## Usage

**Note:** Import leaflet css from `node_modules/leaflet/dist/leaflet.css`

### Svelte action

```html
<!-- map constructor options can be passed as action params -->
<div class="map" use:leafletMap={{ center: [51.505, -0.09], zoom: 13 }} />

<style>
  .map {
    width: 100%;
    height: 100%;
  }
</style>

<script>
  import { tileLayer } from 'leaflet'
  import { makeLeafletMap } from 'leaflet-svelte'

  const leafletMap = makeLeafletMap(map => {
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map)

    map.on('click', event => {
      // do something
    })
  })
</script>
```

### Svelte component

```html
<div class="map"
  <Map on:ready="{onMapReady}" center={[51.505, -0.09]} zoom={13} {...otherMapOptions} />
</div>

<style>
  .map {
    height: 180px;
  }
</style>

<script>
  import { Map } from 'leaflet-svelte'
  import { tileLayer } from 'leaflet'

  function onMapReady(({ detail: map }) => {
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map)

    map.on('click', event => {
      // do something
    })
  })
</script>
```

## Learn more

Find out more about Leaflet at [leafletjs.com](https://leafletjs.com).
