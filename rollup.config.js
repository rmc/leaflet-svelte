import svelte from 'rollup-plugin-svelte'
import resolve from '@rollup/plugin-node-resolve'
import pkg from './package.json'

export default {
  input: 'src/index.js',
  externals: ['leaflet'],
  output: [
    { file: pkg.module, format: 'es' },
    {
      file: pkg.main,
      format: 'umd',
      name: 'LeafletSvelte',
      globals: { leaflet: 'L' },
    },
  ],
  plugins: [svelte(), resolve()],
}
