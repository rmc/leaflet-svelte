import { map as createMap } from 'leaflet'

export default function makeLeafletMap(cb) {
  return function leafletMap(el, opts) {
    const map = createMap(el, opts)
    cb(map)
    return {
      destroy: () => {
        map.remove()
      },
    }
  }
}
