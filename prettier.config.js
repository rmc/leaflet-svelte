module.exports = {
  svelteSortOrder: 'markup-styles-scripts',
  semi: false,
  singleQuote: true,
  trailingComma: 'all',
  overrides: [
    {
      files: '*.svelte',
      plugins: ['svelte'],
    },
  ],
}
