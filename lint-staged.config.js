module.exports = {
  '*.{js,svelte}': ['prettier --write', 'eslint --fix'],
}
